module gitlab.com/opennota/unixtransport

go 1.16

require (
	github.com/miekg/dns v1.1.58
	github.com/oklog/run v1.1.0
	github.com/peterbourgon/ff/v3 v3.4.0
)
