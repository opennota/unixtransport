# unixtransport [![Go Reference](https://pkg.go.dev/badge/gitlab.com/opennota/unixtransport.svg)](https://pkg.go.dev/gitlab.com/opennota/unixtransport)

This package adds support for Unix domain sockets in Go HTTP clients.

```go
t := &http.Transport{...}

unixtransport.Register(t)

client := &http.Client{Transport: t}
```

Now you can make requests with URLs like this:

```go
resp, err := client.Get("https+unix:///path/to/socket:/request/path?a=b")
```

or

``` go
resp, err := client.Get("https+unix://@abstract/2:/request/path?a=b")
```

Use scheme `http+unix` or `https+unix`.

Inspiration taken from, and thanks given to, both
[tv42/httpunix](https://github.com/tv42/httpunix) and
[agorman/httpunix](https://github.com/agorman/httpunix).
